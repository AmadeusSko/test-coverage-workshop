// Goal: assign students to seats
// Usage: node assign-seats.js
// Context: UNC COMP 523 (https://comp523.cs.unc.edu)
// Exercise: refactor to manage effects better


////////////////////      pure utility functions      ////////////////////

const identity = (x) => x
const decrement = (x) => x - 1
const complement = (f) => (x) => !f(x)

const bisect = (items, itemPredicate) =>
  [
    items.filter(itemPredicate),
    items.filter(complement(itemPredicate))
  ]

const updateObject = (object, property, updateValueFunction) =>
  Object.assign({}, object, {
    [property]: updateValueFunction(object[property]),
  })

const groupByFirst = (propertyValuePairs) => {
  const updateGroups = (object, [property, value]) =>
    updateObject(object, property, values => (values || []).concat(value))
  return propertyValuePairs.reduce(updateGroups, {})
}


////////////////////      functional core      ////////////////////

const makePref = ([pid, section, isVip]) => ({pid, section, isVip})
const makePrefs = (rows) => rows.map(makePref)

const updateSeatsBySection = (seatsBySection, pref) => {
  const {section} = pref
  const gotPreference = seatsBySection[section] > 0
  const updateSeatCount = gotPreference ? decrement : identity
  return updateObject(seatsBySection, section, updateSeatCount)
}

const updateAssignments = (seatsBySection, assignments, pref) => {
  const {pid, section} = pref
  const gotPreference = seatsBySection[section] > 0
  const assignedSection = gotPreference ? section : 'overflow'
  const newAssignment = [assignedSection, pid]
  const newAssignments = assignments.concat([newAssignment])
  return newAssignments
}

const assignSeat = ([seatsBySection, assignments], pref) =>
  [
    updateSeatsBySection(seatsBySection, pref),
    updateAssignments(seatsBySection, assignments, pref)
  ]

const assignSeats = (seatsBySection, prefs) =>
  prefs.reduce(assignSeat, [seatsBySection, []])

const assignAllSeats = (seatsBySection, prefs) => {
  const [vips, nonVips] = bisect(prefs, pref => pref.isVip)
  const [newSeatsBySection, vipAssignments] =
    assignSeats(seatsBySection, vips)
  const [finalSeatsBySection, nonVipAssignments] =
    assignSeats(newSeatsBySection, nonVips)
  return vipAssignments.concat(nonVipAssignments)
}


////////////////////      imperative shell      ////////////////////

const main = () => {
  const prefs = makePrefs([
    ['111222333', 'front', false],
    ['111222444', 'front', true],
    ['111222555', 'middle', false],
    ['111222666', 'front', true],
    ['111222777', 'back', false],
    ['111222888', 'back', true],
  ])

  const seatsBySection = {
    'front': 2,
    'middle': 2,
    'back': 2,
  }

  const assignments = assignAllSeats(seatsBySection, prefs)
  const assignmentsBySection = groupByFirst(assignments)

  for (var section in assignmentsBySection) {
    const pidsStr = assignmentsBySection[section].join(', ')
    console.log(`PIDs in '${section}' section: ` + pidsStr)
  }
}


////////////////////      exported functions      ////////////////////

module.exports = {
  bisect,
  main,
}
